<%--
  Created by IntelliJ IDEA.
  User: vincent
  Date: 17/11/2020
  Time: 12:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
    <script src="https://openlayers.org/en/v6.4.3/build/ol.js"></script>
</head>
<body>
<div style="height: 15%">
    Changer la fonction d'une salle :
    <form method="post">
        Salle :
        <select name="salleId_fonction">
            <optgroup label="Rez de chaussé">
                <c:forEach items="${sallesRdc}" var="salle">
                    <option value="${salle.getId()}">${salle.getFonction()} (${salle.getCategorie()})</option>
                </c:forEach>
            </optgroup>
            <optgroup label="Premier étage">
                <c:forEach items="${sallesEtage1}" var="salle">
                    <option value="${salle.getId()}">${salle.getFonction()} (${salle.getCategorie()})</option>
                </c:forEach>
            </optgroup>
        </select>
        Nouvelle fonction :
        <input type="text" name="fonction">
        <button type="submit" name="TODO" value="changerFonction">Changer la fonction</button>
    </form>

    Tracer un chemin vers une salle :
    <form method="post">
        Salle :
        <select name="salleId_chemin">
            <optgroup label="Rez de chaussé">
                <c:forEach items="${sallesRdc}" var="salle">
                    <option value="${salle.getId()}">${salle.getFonction()} (${salle.getCategorie()})</option>
                </c:forEach>
            </optgroup>
            <optgroup label="Premier étage">
                <c:forEach items="${sallesEtage1}" var="salle">
                    <option value="${salle.getId()}">${salle.getFonction()} (${salle.getCategorie()})</option>
                </c:forEach>
            </optgroup>
        </select>
        <button type="submit" name="TODO" value="tracerChemin">Tracer le chemin</button>
        <form method="post">
            <button type="submit" name="TODO" value="effacerChemin">Effacer le chemin </button>
        </form>
    </form>

    <form method="post">
        <button type="submit" name="TODO" value="goToPremier">Aller au premier étage</button>
    </form>
</div>

<div id="map" class="map" style="height: 80%"></div>

<div style="height: 5%">
    <form style="display: flex; align-items: center" method="post">
        <div style="margin-right: 5px" id="coordXi">Position : x=</div>
        <div style="margin-right: 5px" id="coordYi">y=</div>
        <input hidden type="text" name="coordX" id="coordX">
        <input hidden type="text" name="coordY" id="coordY">
        <button style="margin-right: 5px" type="submit" name="TODO" value="changerPosition">Changer la position</button>
    </form>
    URL QRCode : http://192.168.1.30:8080/sig/plan?TODO=changerPosition&coordX=<a id="coordXa"></a>&coordY=<a id="coordYa"></a>&etage=<a id="etagePos">${pos.getEtage()}</a>
</div>


<script type="text/javascript">
    let etagePos = document.getElementById("etagePos");
    let coordX = document.getElementById("coordX");
    let coordY = document.getElementById("coordY");
    let coordXi = document.getElementById("coordXi");
    let coordYi = document.getElementById("coordYi");
    let coordXa = document.getElementById("coordXa");
    let coordYa = document.getElementById("coordYa");
    let xpos = ${pos.getPoint().getX()};
    let ypos = ${pos.getPoint().getY()};

    coordX.value = xpos;
    coordY.value = ypos;
    coordXi.innerText = "Position : x=" + xpos;
    coordYi.innerText = "y=" + ypos;
    coordXa.innerText = xpos.toString();
    coordYa.innerText = ypos.toString();

    let url = 'http://localhost:8081/geoserver/Projet/wms';

    let rdc= new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url: url,
            params: {'LAYERS': 'Projet:salleRDC'},
            serverType: 'geoserver'
        })
    });


    let acces = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url: url,
            params: {'LAYERS': 'Projet:accesRDC'},
            serverType: 'geoserver'
        })
    });

    let chemin = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url: url,
            params: {'LAYERS': 'Projet:cheminRDC'},
            serverType: 'geoserver'
        })
    });

    let position = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url: url,
            params: {'LAYERS': 'Projet:positionRDC'},
            serverType: 'geoserver'
        })
    });

    let view = new ol.View({
        center: [xpos,ypos],
        zoom: 20
    });

    let map = new ol.Map({
        target: 'map',
        layers: [rdc, acces, chemin, position],
        view: view
    });

    map.on('singleclick', function(evt) {
        let x = Math.round(evt.coordinate[0]);
        let y = Math.round(evt.coordinate[1]);
        coordX.value = x;
        coordY.value = y;
        coordXi.innerText = "Position : x=" + x;
        coordYi.innerText = "y=" + y;
        coordXa.innerText = x.toString();
        coordYa.innerText = y.toString();
        etagePos.innerText = "rdc";
    });

</script>

</body>
</html>
