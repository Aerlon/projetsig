package services;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import entities.Acces;
import entities.Chemin;
import entities.Position;
import entities.Salle;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import java.util.*;
import java.util.List;


@Service
public class Services {

    @PersistenceContext
    EntityManager em;

    GeometryFactory geometryFactory = new GeometryFactory();

    public Position getPosition(){
        return em.find(Position.class, 0);
    }

    @Transactional
    public void setPosition(String x, String y, String etage){
        if (!x.equals("") && !y.equals("")){
            int intx = Integer.parseInt(x);
            int inty = Integer.parseInt(y);
            Position pos = getPosition();
            pos.setEtage(etage);
            pos.setPoint(geometryFactory.createPoint(new Coordinate(intx,inty)));
        }
    }

    public List<Salle> getSallesPremierEtage() {
       return em.createQuery("select s from Salle s where s.etage=:premier", Salle.class).setParameter("premier", "premier").getResultList();
    }

    public List<Salle> getSallesRdc() {
        return em.createQuery("select s from Salle s where s.etage=:rdc", Salle.class).setParameter("rdc","rdc").getResultList();
    }

    public List<Salle> getSalles() {
        return em.createQuery("select s from Salle s", Salle.class).getResultList();
    }

    public Salle getSalle(int id) {
        try {
            return em.createQuery("select s from Salle s where s.id_salle=:id", Salle.class).setParameter("id", id).getSingleResult();
        } catch(NoResultException e){
            return null;
        }
    }

    public Acces getAcces(int id_salle1, int id_salle2){
        return em.createQuery("select a from Acces a where (a.id_salle1=:id_salle1 and a.id_salle2=:id_salle2) or (a.id_salle1=:id_salle2 and a.id_salle2=:id_salle1)", Acces.class).setParameter("id_salle1", id_salle1).setParameter("id_salle2", id_salle2).getSingleResult();
    }

    public List<Salle> getSallesAccessibles(int id_salle){
        List<Acces> accesSalle = em.createQuery("select a from Acces a where a.id_salle1=:id or a.id_salle2=:id", Acces.class).setParameter("id",id_salle).getResultList();
        ArrayList<Salle> sallesAccessibles = new ArrayList<>();
        for (Acces a : accesSalle){
            if (a.getId_salle1() == id_salle){
                sallesAccessibles.add(getSalle(a.getId_salle2()));
            }
            else if (a.getId_salle2() == id_salle){
                sallesAccessibles.add(getSalle(a.getId_salle1()));
            }
        }
        return sallesAccessibles;
    }

    public Salle getSallePosition(){
        Position pos = getPosition();
        List<Salle> sallesEtage;
        String etage = pos.getEtage();
        switch (etage){
            case "rdc":
                sallesEtage = getSallesRdc();
                break;
            case "premier":
                sallesEtage = getSallesPremierEtage();
                break;
            default:
                sallesEtage = new ArrayList<>();
        }
        for (Salle s : sallesEtage){
            if (s.getGeom().covers(pos.getPoint())){
                return s;
            }
        }
        return null;
    }

    @Transactional
    public void changerFonctionSalle(int idSalle, String fonc){
        if (!fonc.equals("")){
            Salle salle = getSalle(idSalle);
            if (salle != null){
                salle.setFonction(fonc);
            }
        }
    }

    //parcours en largeur avec sauvegarde des précédents de chaque noeuds dans un dictionnaire
    public Map<Integer,Integer> getMapParents(int id_salle){
        Map<Integer, Integer> parents = new HashMap<>();

        List<Integer> queue = new ArrayList<>();
        List<Integer> discovered = new ArrayList<>();
        Salle salleDepart = getSallePosition();
        queue.add(salleDepart.getId());
        discovered.add(salleDepart.getId());
        boolean found = false;
        while (!queue.isEmpty() && !found){
            int actuel = queue.remove(0);
            if (actuel==id_salle){
                found=true;
            }
            else{
                for (Salle voisin : getSallesAccessibles(actuel)){
                    if (!discovered.contains(voisin.getId())){
                        parents.put(voisin.getId(), actuel);
                        discovered.add(voisin.getId());
                        queue.add(voisin.getId());
                    }
                }
            }
        }
        return parents;
    }

    //récupère le chemin salle par salle entre la position et la salle ayant l'id id_salle
    public List<Salle> getCheminSalle(int id_salle){
        Map<Integer,Integer> parents = getMapParents(id_salle);
        List<Salle> chemin = new ArrayList<>();
        chemin.add(getSalle(id_salle));

        while (parents.containsKey(id_salle)){
            id_salle = parents.get(id_salle);
            chemin.add(getSalle(id_salle));
        }
        chemin.set(chemin.size()-1, getSallePosition());

        return chemin;
    }

    //reconstruit le chemin pour chaque étage point par point à partir du chemin salle par salle
    public Map<String, List<Point>> getChemin(int id_salle){
        List<Salle> cheminSalle = getCheminSalle(id_salle);
        Map<String, List<Point>> chemin = new HashMap<>();
        for (int i=0; i<cheminSalle.size()-1; i++){
            Salle sallei = cheminSalle.get(i);
            if (!chemin.containsKey(sallei.getEtage())){
                chemin.put(sallei.getEtage(), new ArrayList<>());
            }
            Salle salleSuivante = cheminSalle.get(i+1);
            Acces accesi = getAcces(sallei.getId(),salleSuivante.getId());
            chemin.get(sallei.getEtage()).add(accesi.getPoint());
            if (!sallei.getEtage().equals(salleSuivante.getEtage())){
                if (!chemin.containsKey(salleSuivante.getEtage())){
                    chemin.put(salleSuivante.getEtage(), new ArrayList<>());
                }
                chemin.get(salleSuivante.getEtage()).add(accesi.getPoint());
            }

        }
        chemin.get(getPosition().getEtage()).add(getPosition().getPoint());
        return chemin;
    }

    @Transactional
    public void afficherChemin(int id_salle){
        if (id_salle!=getSallePosition().getId()){
            dropChemin();
            Map<String, List<Point>> chemins = getChemin(id_salle);
            for (Map.Entry<String, List<Point>> mapEntry : chemins.entrySet()) {
                List<Coordinate> coords = new ArrayList<>();
                for (Point point : mapEntry.getValue()){
                    coords.add(point.getCoordinate());
                }
                Coordinate[] coordsArray = new Coordinate[coords.size()];
                coordsArray = coords.toArray(coordsArray);
                LineString line = geometryFactory.createLineString(coordsArray);
                Chemin chemin = new Chemin();
                chemin.setChemin(line);
                chemin.setEtage(mapEntry.getKey());
                em.persist(chemin);
            }
        }
    }

    @Transactional
    public void dropChemin(){
        em.createQuery("DELETE FROM Chemin").executeUpdate();
    }

}
