package servlets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import services.Services;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Controller extends HttpServlet {

    @Autowired
    private Services services;

    private String url = "plan";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String etage=request.getParameter("etage");
        if (etage==null) { etage="rdc"; }

        String todo=request.getParameter("TODO");
        if (todo==null) {
            todo="";
            request.setAttribute("sallesRdc", services.getSallesRdc());
            request.setAttribute("sallesEtage1", services.getSallesPremierEtage());
            request.setAttribute("pos", services.getPosition());
            request.getRequestDispatcher("/WEB-INF/"+etage+".jsp").forward(request, response);
        }

        switch (todo) {
            case "changerPosition":
                String coordX = request.getParameter("coordX");
                String coordY = request.getParameter("coordY");
                services.setPosition(coordX,coordY,etage);
                response.sendRedirect(url+"?etage="+etage);
                break;
            case "changerFonction" :
                int salleId_fonction = Integer.parseInt(request.getParameter("salleId_fonction"));
                String nouvelleFonction = request.getParameter("fonction");
                services.changerFonctionSalle(salleId_fonction, nouvelleFonction);
                response.sendRedirect(url+"?etage="+etage);
                break;
            case "goToRdc" :
                response.sendRedirect(url+"?etage=rdc");
                break;
            case "goToPremier" :
                response.sendRedirect(url+"?etage=premier");
                break;
            case "tracerChemin":
                int salleId_chemin = Integer.parseInt(request.getParameter("salleId_chemin"));
                services.afficherChemin(salleId_chemin);
                response.sendRedirect(url+"?etage="+etage);
                break;
            case "effacerChemin":
                services.dropChemin();
                response.sendRedirect(url+"?etage="+etage);
                break;
        }
    }
}
