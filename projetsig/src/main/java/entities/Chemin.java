package entities;

import com.vividsolutions.jts.geom.LineString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Chemin {
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id_chemin;
    private String etage;
    private LineString chemin;

    public Chemin() {
    }

    public void setId_chemin(int id_chemin) {
        this.id_chemin = id_chemin;
    }

    public int getId_chemin() {
        return id_chemin;
    }

    public String getEtage() {
        return etage;
    }

    public void setEtage(String etage) {
        this.etage = etage;
    }

    public LineString getChemin() {
        return chemin;
    }

    public void setChemin(LineString chemin) {
        this.chemin = chemin;
    }
}
