package entities;

import com.vividsolutions.jts.geom.Point;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Acces {
    @Id
    private int id_acces;
    private String etage;
    private int id_salle1;
    private int id_salle2;
    private Point point;

    public Acces(){
    }

    public int getId() {
        return id_acces;
    }

    public String getEtage() {
        return etage;
    }

    public void setEtage(String etage) {
        this.etage = etage;
    }

    public int getId_salle1() {
        return id_salle1;
    }

    public void setId_salle1(int id_salle1) {
        this.id_salle1 = id_salle1;
    }

    public int getId_salle2() {
        return id_salle2;
    }

    public void setId_salle2(int id_salle2) {
        this.id_salle2 = id_salle2;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }
}
