package entities;

import javax.persistence.*;

import com.vividsolutions.jts.geom.Point;


@Entity
public class Position {
    @Id
    private int id_position;
    private String etage;
    private Point point;

    public Position(){
        this.id_position=0;
    }

    public String getEtage() {
        return etage;
    }

    public void setEtage(String etage) {
        this.etage = etage;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }
}
