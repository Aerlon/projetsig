CREATE TABLE SALLE (id_salle INT NOT NULL, etage VARCHAR(30), categorie VARCHAR(30), fonction VARCHAR(50), geom geometry(MultiPolygon), referencepoint geometry(Point), PRIMARY KEY (id_salle));

CREATE TABLE POSITION (id_position INT NOT NULL, etage VARCHAR(30), point geometry(Point), PRIMARY KEY (id_position));

CREATE TABLE ACCES (id_acces INT NOT NULL, etage VARCHAR(30), id_salle1 INT NOT NULL, id_salle2 INT NOT NULL, point geometry(Point), PRIMARY KEY (id_acces), FOREIGN KEY (id_salle1) REFERENCES SALLE (id_salle), FOREIGN KEY (id_salle2) REFERENCES SALLE (id_salle));

CREATE TABLE CHEMIN (id_chemin INT NOT NULL, etage VARCHAR(30), chemin geometry(LineString), PRIMARY KEY (id_chemin));
