
INSERT INTO SALLE VALUES (1, 'rdc', 'TP', 'Salle de TP E01', 'MULTIPOLYGON(((0 0, 30 0, 30 30, 0 30, 0 0)))');
INSERT INTO SALLE VALUES (2, 'rdc', 'TP', 'Salle de TP E02', 'MULTIPOLYGON(((30 0, 70 0, 70 30, 30 30, 30 0)))');
INSERT INTO SALLE VALUES (3, 'rdc', 'TP', 'Salle de TP E03', 'MULTIPOLYGON(((0 40, 30 40, 30 70, 0 70, 0 40)))');
INSERT INTO SALLE VALUES (4, 'rdc', 'TP', 'Salle de TP E04', 'MULTIPOLYGON(((30 40, 60 40, 60 70, 30 70, 30 40)))');
INSERT INTO SALLE VALUES (5, 'rdc', 'TD', 'Salle de TD E05', 'MULTIPOLYGON(((70 40, 100 40, 100 70, 70 70, 70 40)))');
INSERT INTO SALLE VALUES (6, 'rdc', 'TD', 'Salle de TD E06', 'MULTIPOLYGON(((70 70, 100 70, 100 100, 70 100, 70 70)))');
INSERT INTO SALLE VALUES (7, 'rdc', 'CM', 'Amphithéatre 1', 'MULTIPOLYGON(((-80 0, 0 0, 0 50, -80 50, -80 0)))');
INSERT INTO SALLE VALUES (8, 'rdc', 'CM', 'Amphithéatre 2', 'MULTIPOLYGON(((-80 50, 0 50, 0 100, -80 100, -80 50)))');
INSERT INTO SALLE VALUES (9, 'rdc', 'bureau', 'Bureau 01', 'MULTIPOLYGON(((0 80, 25 80, 25 100, 0 100, 0 80)))');
INSERT INTO SALLE VALUES (10, 'rdc', 'bureau', 'Bureau 02', 'MULTIPOLYGON(((25 80, 50 80, 50 100, 25 100, 25 80)))');
INSERT INTO SALLE VALUES (11, 'rdc', 'bureau', 'Bureau des associations', 'MULTIPOLYGON(((50 80, 70 80, 70 100, 50 100, 50 80)))');
INSERT INTO SALLE VALUES (12, 'rdc', 'wc', 'WC', 'MULTIPOLYGON(((65 0, 85 0, 85 30, 65 30, 65 0)))', 'Point(75 20)');
INSERT INTO SALLE VALUES (13, 'rdc', 'couloir', 'Couloir 1', 'MULTIPOLYGON(((0 30, 100 30, 100 40, 0 40, 0 30)))');
INSERT INTO SALLE VALUES (14, 'rdc', 'couloir', 'Couloir 2', 'MULTIPOLYGON(((60 40, 70 40, 70 70, 60 70, 60 40)))');
INSERT INTO SALLE VALUES (15, 'rdc', 'couloir', 'Couloir 3', 'MULTIPOLYGON(((0 70, 70 70, 70 80, 0 80, 0 70)))');
INSERT INTO SALLE VALUES (16, 'rdc', 'escalier', 'Escalier', 'MULTIPOLYGON(((85 0, 100 0, 100 30, 85 30, 85 0)))');


INSERT INTO SALLE VALUES (17, 'premier', 'TP', 'Salle de TP E12', 'MULTIPOLYGON(((0 0, 35 0, 35 30, 0 30, 0 0)))');
INSERT INTO SALLE VALUES (18, 'premier', 'TP', 'Salle de TP E13', 'MULTIPOLYGON(((35 0, 70 0, 70 30, 35 30, 35 0)))');
INSERT INTO SALLE VALUES (19, 'premier', 'TD', 'Salle de TD E14', 'MULTIPOLYGON(((0 70, 45 70, 45 100, 0 100, 0 70)))');
INSERT INTO SALLE VALUES (20, 'premier', 'TD', 'Salle de TD E15', 'MULTIPOLYGON(((0 40, 45 40, 45 70, 0 70, 0 40)))');
INSERT INTO SALLE VALUES (21, 'premier', 'TD', 'Salle de TD E16', 'MULTIPOLYGON(((55 70, 100 70, 100 100, 55 100, 55 70)))');
INSERT INTO SALLE VALUES (22, 'premier', 'TD', 'Salle de TD E17', 'MULTIPOLYGON(((55 40, 100 40, 100 70, 55 70, 55 40)))');
INSERT INTO SALLE VALUES (23, 'premier', 'wc', 'WC', 'MULTIPOLYGON(((65 0, 85 0, 85 30, 65 30, 65 0)))');
INSERT INTO SALLE VALUES (24, 'premier', 'couloir', 'Couloir 1', 'MULTIPOLYGON(((0 30, 100 30, 100 40, 0 40, 0 30)))');
INSERT INTO SALLE VALUES (25, 'premier', 'couloir', 'Couloir 2', 'MULTIPOLYGON(((45 40, 55 40, 55 100, 45 100, 45 40)))');
INSERT INTO SALLE VALUES (26, 'premier', 'escalier', 'Escalier', 'MULTIPOLYGON(((85 0, 100 0, 100 30, 85 30, 85 0)))');

INSERT INTO POSITION VALUES (0, 'rdc', 'Point(3 18)');

INSERT INTO ACCES VALUES (0, 'rdc', 7, 13, 'Point(0 35)');
INSERT INTO ACCES VALUES (1, 'rdc', 1, 2, 'Point(30 15)');
INSERT INTO ACCES VALUES (2, 'rdc', 1, 13, 'Point(15 30)');
INSERT INTO ACCES VALUES (3, 'rdc', 2, 13, 'Point(50 30)');
INSERT INTO ACCES VALUES (4, 'rdc', 12, 13, 'Point(75 30)');
INSERT INTO ACCES VALUES (5, 'rdc', 16, 13, 'Point(92 30)');
INSERT INTO ACCES VALUES (6, 'rdc', 3, 13, 'Point(15 40)');
INSERT INTO ACCES VALUES (7, 'rdc', 4, 13, 'Point(45 40)');
INSERT INTO ACCES VALUES (8, 'rdc', 13, 14, 'Point(65 40)');
INSERT INTO ACCES VALUES (9, 'rdc', 5, 13, 'Point(85 40)');
INSERT INTO ACCES VALUES (10, 'rdc', 6, 15, 'Point(70 75)');
INSERT INTO ACCES VALUES (11, 'rdc', 14, 15, 'Point(65 70)');
INSERT INTO ACCES VALUES (12, 'rdc', 11, 15, 'Point(60 80)');
INSERT INTO ACCES VALUES (13, 'rdc', 10, 15, 'Point(37 80)');
INSERT INTO ACCES VALUES (14, 'rdc', 9, 15, 'Point(12 80)');
INSERT INTO ACCES VALUES (15, 'rdc', 8, 15, 'Point(0 75)');


INSERT INTO ACCES VALUES (16, 'premier', 17, 24, 'Point(17 30)');
INSERT INTO ACCES VALUES (17, 'premier', 18, 24, 'Point(50 30)');
INSERT INTO ACCES VALUES (18, 'premier', 23, 24, 'Point(75 30)');
INSERT INTO ACCES VALUES (19, 'premier', 26, 24, 'Point(92 30)');
INSERT INTO ACCES VALUES (20, 'premier', 24, 25, 'Point(50 40)');
INSERT INTO ACCES VALUES (21, 'premier', 19, 25, 'Point(45 85)');
INSERT INTO ACCES VALUES (22, 'premier', 20, 25, 'Point(45 55)');
INSERT INTO ACCES VALUES (23, 'premier', 21, 25, 'Point(55 85)');
INSERT INTO ACCES VALUES (24, 'premier', 22, 25, 'Point(55 55)');

INSERT INTO ACCES VALUES (25, 'rdc premier', 16, 26, 'Point(92 15)');


