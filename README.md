**Projet SIG - Vincent Deschamps, Martin Schultz**

Projet Spring/jpa déployé sur localhost:8080/projetsig_war/plan

Ce projet nécessite une base de données Postgres/postgis déployée sur localhost:5432.
L'identifiant et le mot de passe sont à modifier dans projetsig/src/main/configuration/ClientWebConfig.java.
Une database "batiment" doit avoir été créée

Ce projet demande également un geoserver tournant sur localhost:8081. 
Un espace de travail "Projet" est créé sur ce geoserver et dans lequel sont publiées 8 couches :
- positionPREMIER et positionRDC depuis la table position de la BD et avec les filtres respectifs "etage = 'premier'" et "etage = 'rdc'"
- sallePREMIER et salleRDC depuis la table salle de la BD et avec les filtres respectifs "etage = 'premier'" et "etage = 'rdc'"
- accesPREMIER et accesRDC depuis la table acces de la BD et avec les filtres respectifs "etage = 'premier'" et "etage = 'rdc'"
- cheminPREMIER et cheminRDC depuis la table chemin de la BD et avec les filtres respectifs "etage = 'premier'" et "etage = 'rdc'"

Les couches concernant la postion, les salles et les acces ont respectivement pour style SLD les fichiers Autres/geoserverPositionStyle.sld, 
Autres/geoserverSalleStyle.sld et Autres/geoserverAccesStyle.sld